package com.rolan.wordguess.game.activity;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.rolan.wordguess.R;
import com.rolan.wordguess.game.adapter.UsersListAdapter;
import com.rolan.wordguess.model.User;
import com.rolan.wordguess.model.Word;
import com.rolan.wordguess.storage.DBOpenHelper;
import com.rolan.wordguess.storage.UsersDBAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameUsersActivity extends AppCompatActivity {

    private ListView users_list;
    private String uid = " ";
    private String name = " ";
    private String gender = " ";
    private String email = " ";
    private String coins = " ";
    private Boolean online = true;
    private UsersDBAdapter usersDBAdapter;
    private DBOpenHelper dbOpenHelper;
    private String user_id, user_name, user_gender, user_email, user_coins, user_online;
    private User user;
    private List<User> items = new ArrayList<>();
    private UsersListAdapter usersListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_game_users);

        usersDBAdapter = new UsersDBAdapter(this);

        fetchUsers();

        users_list = (ListView)findViewById(R.id.users_list);

        if(usersDBAdapter.getRowCount() > 0){
            items = getValues();

            usersListAdapter = new UsersListAdapter(this, R.layout.user_list_item, items);
            users_list.setAdapter(usersListAdapter);
        }
    }

    public void fetchUsers(){
        Firebase ref = new Firebase("https://pinoygenius.firebaseio.com/Users");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snapshot, String previousChild) {
                User user = snapshot.getValue(User.class);

//                private String uid = " ";
//                private String name = " ";
//                private String gender = " ";
//                private String email = " ";
//                private String coins = " ";
//                private Boolean online = true;
                uid = user.getUid();
                name = user.getName();
                gender = user.getGender();
                email = user.getEmail();
                coins = user.getCoins();
                online = user.getOnline();

                usersListAdapter.notifyDataSetChanged();
                usersDBAdapter.updateUserDEtails(uid, name, gender, email, coins, String.valueOf(online));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
            // ....
        });
    }

    private List<User> getValues() {
        Log.e("Reading dab", "Long running");
        List<User> resultList = new ArrayList<User>();
        Cursor c = usersDBAdapter.queryAllUsers();
        while (c.moveToNext()) {
            user_id = c.getString(c.getColumnIndex(dbOpenHelper.KEY_UID));
            user_name = c.getString(c.getColumnIndex(dbOpenHelper.KEY_NAME));
            user_gender = c.getString(c.getColumnIndex(dbOpenHelper.KEY_GENDER));
            user_email = c.getString(c.getColumnIndex(dbOpenHelper.KEY_EMAIL));
            user_coins = c.getString(c.getColumnIndex(dbOpenHelper.KEY_COINS));
            user_online = c.getString(c.getColumnIndex(dbOpenHelper.KEY_ONLINE));

            Log.e("message", user_id);
            user = new User();
            user.setUid(user_id);
            user.setName(user_name);
            user.setGender(user_gender);
            user.setEmail(user_email);
            user.setCoins(user_coins);
            user.setOnline(Boolean.valueOf(user_online));

            resultList.add(user);
        }

        c.close();
        return resultList;
    }
}
