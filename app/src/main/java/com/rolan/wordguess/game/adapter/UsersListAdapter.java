package com.rolan.wordguess.game.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.rolan.wordguess.R;
import com.rolan.wordguess.model.User;
import com.rolan.wordguess.util.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Brayo on 4/25/2016.
 */
public class UsersListAdapter extends ArrayAdapter<User> {

    private final LayoutInflater inflater;
    private final SessionManager sessionManager;
    private Context context;
    private List<User> users;
    private User user;
    private String user_name;
    String name;
    private HashMap<String, String> user_session = new HashMap<>();

    public UsersListAdapter(Context context, int textViewResourceId,
                            List<User> objects) {
        super(context, textViewResourceId, objects);
        this.users = objects;
        this.context = context;
        sessionManager = new SessionManager(context);
        user_session = sessionManager.getUserDetails();
        user_name = user_session.get(SessionManager.KEY_NAME);

        if (user_name.contains("@")) {
            String[] n = user_name.split("@");
            name = n[0];
        } else {
            name = user_name;
        }

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolder holder = null;
        user = users.get(position);

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.user_list_item, null);
            holder.user_img = (ImageView) view.findViewById(R.id.user_img);
            holder.user_name = (TextView) view.findViewById(R.id.user_name);
            holder.user_online = (ImageView) view.findViewById(R.id.user_online);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

//        if (user.getName().equals("Anon"))
//            holder.user_name.setText(name);
//        else
            holder.user_name.setText(user.getName());

        if (user.getName().length() > 0) {
            ColorGenerator generator = ColorGenerator.MATERIAL;
            int color = generator.getRandomColor();
            String first_char = user.getName().substring(0,
                    Math.min(user.getName().length(), 1)).toUpperCase();
            TextDrawable drawable = TextDrawable.builder().buildRound(first_char, color);
            holder.user_img.setImageDrawable(drawable);
        }

        holder.user_online.setVisibility(View.GONE);

        if (user.getOnline()) {
            holder.user_online.setVisibility(View.VISIBLE);
        }

        return view;
    }

    public class ViewHolder {
        ImageView user_img;
        TextView user_name;
        ImageView user_online;
    }
}
