package com.rolan.wordguess.main.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.api.attributes.BootstrapBrand;
import com.facebook.login.LoginManager;
import com.firebase.client.AuthData;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.rolan.wordguess.R;
import com.rolan.wordguess.game.activity.GameUsersActivity;
import com.rolan.wordguess.model.Clues;
import com.rolan.wordguess.model.User;
import com.rolan.wordguess.model.Word;
import com.rolan.wordguess.prefs.LettersPref;
import com.rolan.wordguess.storage.UsersDBAdapter;
import com.rolan.wordguess.user.LoginActivity;
import com.rolan.wordguess.user.RegistrationActivity;
import com.rolan.wordguess.util.Constants;
import com.rolan.wordguess.util.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Firebase mFirebaseRef;
    private BootstrapButton single_player, multi_player;
    private LettersPref letterPrefs;
    private SessionManager sessionManager;
    private Firebase ref;
    private String uid;
    private UsersDBAdapter usersDBAdapter;
    private ArrayList<Boolean> user_exist = new ArrayList<>();
    private String user_id = "";
    private AdView mAdView;
    private ImageView profile_image;
    private TextView profile_name, profile_coins, profile_level;
    private HashMap<String, String> user_session = new HashMap<>();
    private String user_name;
    private String db_user_name = "";
    private User user = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ref = new Firebase("https://pinoygenius.firebaseio.com/");

        mFirebaseRef = new Firebase(Constants.APP_URL);
        sessionManager = new SessionManager(this);
        usersDBAdapter = new UsersDBAdapter(this);
        single_player = (BootstrapButton) findViewById(R.id.single_player);
        multi_player = (BootstrapButton) findViewById(R.id.multi_player);
        profile_image = (ImageView) findViewById(R.id.user_profile_pic);
        profile_name = (TextView) findViewById(R.id.user_profile_name);
        profile_coins = (TextView) findViewById(R.id.user_profile_coins);
        profile_level = (TextView) findViewById(R.id.user_profile_level);
        //createWord();

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        user_session = sessionManager.getUserDetails();

        user_name = user_session.get(SessionManager.KEY_NAME);

        if (user_name.contains("@")) {
            String[] n = user_name.split("@");
            profile_name.setText(n[0]);
        }else {
            profile_name.setText(user_name);
        }

//        AdRequest adRequest = new AdRequest.Builder()
//                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
//                // Check the LogCat to get your test device ID
//                .addTestDevice("705C3D5DBC14D8EADD3B9107816BD501")
//                .build();
        mAdView.loadAd(adRequest);

        single_player.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, SinglePlayerActivity.class);
//                letterPrefs = new LettersPref(MainActivity.this);
//                letterPrefs.clearAll();
                startActivity(i);
            }
        });

        ref.addAuthStateListener(new Firebase.AuthStateListener() {
            @Override
            public void onAuthStateChanged(AuthData authData) {
                if (authData != null) {
                    Log.e("Uthenticated user", authData.getUid());
                    uid = authData.getUid();

                    Cursor m_cursor = usersDBAdapter.querySingleUser(uid);
                    if (m_cursor.moveToFirst()) {
                        user_id = m_cursor.getString(m_cursor.getColumnIndex("uid"));
                        db_user_name = m_cursor.getString(m_cursor.getColumnIndex("name"));
                    }
                    m_cursor.close();

                    Log.e("Database user value", user_id + " - " + uid);

                    if (user_id.equals(uid)) {
                        usersDBAdapter.updateUser(uid, "true");
                        Firebase ref = new Firebase("https://pinoygenius.firebaseio.com/Users");
                        Firebase userRef = ref.child(uid);
                        Map<String, Object> details = new HashMap<String, Object>();
                        details.put("online", true);
                        userRef.updateChildren(details);
                    } else {
                        usersDBAdapter.insertUser(authData.getUid(), "true");

                        //insert into firebase the users table

                        if (db_user_name.length() == 0 || db_user_name.equals("Anon")) {
                            user.setUid(authData.getUid());
                            user.setOnline(true);

                            mFirebaseRef.child("Users").child(authData.getUid()).setValue(user);
                        }
                    }

                } else {
                    //Toast.makeText(LoginActivity.this, "Oops and error occured", Toast.LENGTH_LONG).show();
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_share) {

        } else if (id == R.id.nav_logout) {
            Firebase ref = new Firebase("https://pinoygenius.firebaseio.com/Users");

            Firebase userRef = ref.child(uid);
            Map<String, Object> details = new HashMap<String, Object>();
            details.put("online", false);
            userRef.updateChildren(details);

            sessionManager.logoutUser();
            ref.unauth();
            LoginManager.getInstance().logOut();

            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_profile) {
            Intent i = new Intent(MainActivity.this, RegistrationActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_users) {
            Intent i = new Intent(MainActivity.this, GameUsersActivity.class);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}
