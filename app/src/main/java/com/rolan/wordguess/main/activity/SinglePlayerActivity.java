package com.rolan.wordguess.main.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.rolan.wordguess.R;
import com.rolan.wordguess.model.Clues;
import com.rolan.wordguess.model.Word;
import com.rolan.wordguess.pagermanager.MainPagerAdapter;
import com.rolan.wordguess.pagermanager.SlidingTabLayout;
import com.rolan.wordguess.prefs.RandomKey;
import com.rolan.wordguess.prefs.RandomWord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class SinglePlayerActivity extends AppCompatActivity {

    private CharSequence Titles[] = {"Game", "Activity"};
    private MainPagerAdapter mCustomPagerAdapter;
    private ViewPager mViewPager;
    private SlidingTabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_player);


        mCustomPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), this, Titles);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.getAdapter().notifyDataSetChanged();
        mViewPager.setAdapter(mCustomPagerAdapter);

        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.textColorPrimary);
            }
        });

        tabs.setViewPager(mViewPager);

    }
}
