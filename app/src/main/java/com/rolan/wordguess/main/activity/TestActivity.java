package com.rolan.wordguess.main.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.rolan.wordguess.R;
import com.rolan.wordguess.model.Clues;
import com.rolan.wordguess.model.Word;
import com.rolan.wordguess.prefs.RandomKey;
import com.rolan.wordguess.prefs.RandomWord;
import com.rolan.wordguess.util.Constants;

import java.util.ArrayList;
import java.util.Random;

public class TestActivity extends AppCompatActivity {
    private Firebase mFirebaseRef;
    private Button start_game;
    private TextView word_txt;
    private EditText user_text;
    private Button answer_btn;
    private ArrayList<String> keys = new ArrayList<>();
    private ArrayList<String> words = new ArrayList<>();
    private ArrayList<String> description = new ArrayList<>();
    private int random_key;
    private Random randomGenerator;
    private RandomKey randomKey;
    private RandomWord randomWord;
    private TextView output;
    String user_answer;
    private boolean answered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //storage
        randomKey = new RandomKey(this);
        randomWord = new RandomWord(this);

        start_game = (Button) findViewById(R.id.start_game);
        word_txt = (TextView) findViewById(R.id.word);
        user_text = (EditText) findViewById(R.id.user_txt);
        answer_btn = (Button) findViewById(R.id.guess);
        output = (TextView)findViewById(R.id.output);

        mFirebaseRef = new Firebase(Constants.APP_URL);

        //createWord();

        start_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGame();

            }
        });

        answer_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user_answer = user_text.getText().toString();

                for(int i = 0; i < description.size(); i++){
                    String pattern = ".*"+ user_answer +".*";
                    if(description.get(i).matches(pattern)) {
                        answered = true;
                        break;
                    }else {
                        answered = false;
                    }
                }

                if(answered){
                    Toast.makeText(TestActivity.this, "Correct", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(TestActivity.this, "Incorrect", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createWord() {

        Firebase postRef = mFirebaseRef.child("Words");
        Firebase newPostRef = postRef.push();

        //like synonyms
        ArrayList<String> desc = new ArrayList<>();
        desc.add("Large");
        desc.add("Mammal");
        desc.add("Produces Milk");

        ArrayList<String> clue = new ArrayList<>();
        clue.add("Domestic Animal");
        clue.add("Herbivore");
        clue.add("Herd");
        clue.add("Shed");
        clue.add("Calf");
        clue.add("Grass");
        clue.add("Horns");
        clue.add("Hide");
        clue.add("Ring");
        clue.add("Bull");

        Word word = new Word();
        word.setWord("Cow");
        word.setDescription_words(desc);
        newPostRef.setValue(word);

        String id = newPostRef.getKey();

        //create clues for this word
        Clues clues = new Clues();
        clues.setWord_id(id);
        clues.setClues(clue);

        mFirebaseRef.child("Clues").push().setValue(clues);
    }

    public void startGame() {
        Firebase ref = new Firebase("https://pinoygenius.firebaseio.com/Words");
        // Query queryRef = ref.orderByChild("dimensions/height");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snapshot, String previousChild) {
                Word word = snapshot.getValue(Word.class);
                keys.add(snapshot.getKey());
                words.add(word.getWord());
                System.out.println(snapshot.getKey() + "  " + word.getWord());

                randomGenerator = new Random();
                random_key = randomGenerator.nextInt(keys.size());
                word_txt.setText(words.get(random_key));
                description = word.getDescription_words();

                //store key and word
                randomWord.createWord(words.get(random_key));
                randomKey.createKey(keys.get(random_key));
                Log.e("Key================================", keys.size() + " " + random_key);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
            // ....
        });


    }
}
