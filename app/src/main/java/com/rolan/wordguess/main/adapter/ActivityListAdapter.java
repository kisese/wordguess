package com.rolan.wordguess.main.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rolan.wordguess.R;
import com.rolan.wordguess.model.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brayo on 4/21/2016.
 */
public class ActivityListAdapter extends ArrayAdapter {

    private Context context;
    private List<Message> items = new ArrayList<>();
    private Message message;

    public ActivityListAdapter(Context context, int resource, List<Message> objects) {
        super(context, resource, objects);
        this.items = objects;
        this.context = context;
    }


    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        message = items.get(position);
        ViewHolder viewHolder = null;
        int listViewItemType = getItemViewType(position);

        if (convertView == null) {
            if (listViewItemType == Message.MY_MESSAGE) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.message_right, null);
            } else{
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.message_left, null);
            }
//            else {
//                convertView = LayoutInflater.from(getContext()).inflate(R.layout.date_layout, null);
//            }

            TextView textView = (TextView) convertView.findViewById(R.id.message_text);
            viewHolder = new ViewHolder(textView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();

        }

        String textViewText = message.getMessage();
        viewHolder.getText().setText(textViewText);

        return convertView;
    }
}
