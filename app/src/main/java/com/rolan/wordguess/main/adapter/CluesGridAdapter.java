package com.rolan.wordguess.main.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.rolan.wordguess.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Brayo on 4/18/2016.
 */
public class CluesGridAdapter  extends ArrayAdapter<String> {

    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
    List<String> clues = new ArrayList<>();
    private final LayoutInflater inflater;
    private Context context;

    public CluesGridAdapter(Context context, int textViewResourceId,
                           List<String> objects) {
        super(context, textViewResourceId, objects);
        this.clues = objects;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < objects.size(); ++i) {
            mIdMap.put(objects.get(i), i);
        }
    }

    @Override
    public long getItemId(int position) {
        String item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder = null;

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.clues_grid_item, null);
            holder.clues_btn = (Button)view.findViewById(R.id.clue_btn);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.clues_btn.setText(clues.get(position));

        return view;
    }

    public class ViewHolder{
        Button clues_btn;
    }
}
