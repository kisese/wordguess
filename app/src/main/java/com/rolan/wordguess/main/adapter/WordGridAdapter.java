package com.rolan.wordguess.main.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.rolan.wordguess.R;
import com.rolan.wordguess.user.LoginActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Brayo on 4/16/2016.
 */
public class WordGridAdapter extends ArrayAdapter<String> {

    HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();
    List<String> letters = new ArrayList<>();
    private final LayoutInflater inflater;
    private Context context;
    String letter;

    public WordGridAdapter(Context context, int textViewResourceId,
                           List<String> objects) {
        super(context, textViewResourceId, objects);
        this.letters = objects;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < objects.size(); ++i) {
            mIdMap.put(objects.get(i), i);
        }
    }

    @Override
    public long getItemId(int position) {
        String item = getItem(position);
        return mIdMap.get(item);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder = null;

        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.letters_grid_item, null);
            holder.letter_btn = (Button) view.findViewById(R.id.letter_btn);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        letter = letters.get(position).toString();
        holder.letter_btn.setText(letter);

        return view;
    }

    public class ViewHolder {
        Button letter_btn;
    }
}

