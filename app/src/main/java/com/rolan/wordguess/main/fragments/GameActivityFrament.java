package com.rolan.wordguess.main.fragments;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.rolan.wordguess.R;
import com.rolan.wordguess.main.adapter.ActivityListAdapter;
import com.rolan.wordguess.model.Message;
import com.rolan.wordguess.storage.DBOpenHelper;
import com.rolan.wordguess.storage.MessagesDbAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Brayo on 4/15/2016.
 */
public class GameActivityFrament extends Fragment {

    public static ListView activity_list;
    public static MessagesDbAdapter messagesDbAdapter;
    public static String id, msg, created_at, updated_at;
    public static int type;
    public static DBOpenHelper dbOpenHelper;
    public static Message message;
    public static List<Message> items = new ArrayList<>();
    public static ActivityListAdapter list_adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        messagesDbAdapter = new MessagesDbAdapter(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_game_activity, container, false);

        activity_list = (ListView)rootView.findViewById(R.id.activity_list);

        items = getValues();

        list_adapter = new ActivityListAdapter(getActivity(), R.id.text, items);
        activity_list.setAdapter(list_adapter);
        return rootView;
    }

    public static List<Message> getValues() {
        List<Message> resultList = new ArrayList<Message>();
        Cursor c = messagesDbAdapter.queryAllMessages();
        while (c.moveToNext()) {
            id = c.getString(c.getColumnIndex(dbOpenHelper.KEY_ID));
            msg = c.getString(c.getColumnIndex(dbOpenHelper.KEY_MESSAGE));
            type = Integer.valueOf(c.getString(c.getColumnIndex(dbOpenHelper.KEY_TYPE)));
            created_at = c.getString(c.getColumnIndex(dbOpenHelper.KEY_CREATED_AT));
            updated_at = c.getString(c.getColumnIndex(dbOpenHelper.KEY_UPDATED_AT));

            Log.e("message", msg);
            message = new Message();
            message.setId(id);
            message.setMessage(msg);
            message.setType(type);
            message.setCreated_at(created_at);
            message.setUpdated_at(updated_at);

            resultList.add(message);
        }

        c.close();
        return resultList;
    }

    public static void refreshList(Context context){
        items = getValues();
        list_adapter = new ActivityListAdapter(context, R.id.text, items);
        activity_list.setAdapter(list_adapter);
        list_adapter.notifyDataSetChanged();
        activity_list.invalidateViews();
    }
}
