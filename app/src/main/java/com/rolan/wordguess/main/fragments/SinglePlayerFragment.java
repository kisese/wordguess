package com.rolan.wordguess.main.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.BaseInputConnection;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.rolan.wordguess.R;
import com.rolan.wordguess.main.adapter.CluesGridAdapter;
import com.rolan.wordguess.main.adapter.WordGridAdapter;
import com.rolan.wordguess.model.Clues;
import com.rolan.wordguess.model.Message;
import com.rolan.wordguess.model.Word;
import com.rolan.wordguess.prefs.LettersPref;
import com.rolan.wordguess.prefs.RandomKey;
import com.rolan.wordguess.prefs.RandomWord;
import com.rolan.wordguess.storage.CluesDBAdapter;
import com.rolan.wordguess.storage.DBOpenHelper;
import com.rolan.wordguess.storage.LettersDBAdapter;
import com.rolan.wordguess.storage.MessagesDbAdapter;
import com.rolan.wordguess.util.GetDate;
import com.rolan.wordguess.util.ListViewHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Brayo on 4/15/2016.
 */
public class SinglePlayerFragment extends Fragment {

    private ArrayList<String> keys = new ArrayList<>();
    private ArrayList<String> words = new ArrayList<>();
    private ArrayList<String> description = new ArrayList<>();
    private List<String> clues_list = new ArrayList<>();
    private List<String> letters_list = new ArrayList<>();
    private int random_index;
    private Random randomGenerator;
    private RandomKey randomKey;
    private RandomWord randomWord;
    String answer_word;
    String current_key;
    String clues_key;
    char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    private GridView letters_grid, clues_grid;
    private WordGridAdapter wordGridAdapter;
    private LettersPref letterPrefs;
    private LettersDBAdapter lettersDBAdapter;
    private CluesDBAdapter cluesDBAdapter;
    private DBOpenHelper dbOpenHelper;
    private String letter_name;
    private String clue_name;
    private CluesGridAdapter cluesGridAdapter;
    private Editable editable;
    private EditText answer_edtxt;
    private ImageButton backspace;
    private BootstrapButton answer_btn;
    private MessagesDbAdapter messagesDbAdapter;
    private GetDate getDate = new GetDate();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //storage
        randomKey = new RandomKey(getActivity());
        randomWord = new RandomWord(getActivity());
        letterPrefs = new LettersPref(getActivity());
        lettersDBAdapter = new LettersDBAdapter(getActivity());
        cluesDBAdapter = new CluesDBAdapter(getActivity());
        messagesDbAdapter = new MessagesDbAdapter(getActivity());


        startGame();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_single_player, container, false);

        letters_grid = (GridView) rootView.findViewById(R.id.letters_grid);
        clues_grid = (GridView) rootView.findViewById(R.id.clues_grid);
        answer_edtxt = (EditText) rootView.findViewById(R.id.answer_edtxt);
        backspace = (ImageButton)rootView.findViewById(R.id.backspace_btn);
        answer_btn = (BootstrapButton)rootView.findViewById(R.id.btn_answer);

        answer_edtxt.setFilters(new InputFilter[]{new InputFilter.AllCaps(),
                new InputFilter.LengthFilter(16)});

        //letters_list = getValues();
        //clues_list = getClues();
        editable = answer_edtxt.getText();

        final BaseInputConnection textFieldInputConnection = new BaseInputConnection(answer_edtxt, true);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textFieldInputConnection.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL));
                editable = answer_edtxt.getText();
            }
        });

        try {

            answer_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(answer_edtxt.getText().length() > 0){
                        String word = randomWord.getWord();
                        String user_word = answer_edtxt.getText().toString();

                        messagesDbAdapter.insertMessage(user_word, String.valueOf(Message.MY_MESSAGE));
                       // messagesDbAdapter.insertMessage(getDate.getTimeStamp(), String.valueOf(Message.DATE));

                        if(user_word.equalsIgnoreCase(word)){
                            String message = "You are correct, the answer is \n " + word;
                            messagesDbAdapter.insertMessage(message,
                                    String.valueOf(Message.NOT_MY_MESSAGE));

                            new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Good job!")
                                    .setContentText(message)
                                    .show();

                            startGame();
                            fetchClues();
                        }else {
                            String message = "Wrong answer, try your luck again";
                            messagesDbAdapter.insertMessage("Wrong answer, try your luck again",
                                    String.valueOf(Message.NOT_MY_MESSAGE));

                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Are you sure?")
                                    .setContentText("Wrong answer, try your luck again")
                                    .show();
                        }
                    }

                    GameActivityFrament.refreshList(getActivity());
                }
            });

            letters_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    editable.append(letters_list.get(position));
                    answer_edtxt.setText(editable);
                    answer_edtxt.setSelection(answer_edtxt.getText().length());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rootView;
    }

    //fect clues
    public void startGame() {
        final ArrayList<Character> charList = new ArrayList<Character>();
        Firebase ref = new Firebase("https://pinoygenius.firebaseio.com/Words");
        // Query queryRef = ref.orderByChild("dimensions/height");
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot snapshot, String previousChild) {
                Word word = snapshot.getValue(Word.class);
                keys.add(snapshot.getKey());
                words.add(word.getWord());
                System.out.println(snapshot.getKey() + "  " + word.getWord());

                randomGenerator = new Random();
                random_index = randomGenerator.nextInt(keys.size());

                answer_word = words.get(random_index);
                current_key = keys.get(random_index);

                Log.e("Play data===", answer_word + " " + current_key);

                description = word.getDescription_words();
                getLettersList(answer_word);

                //store key and word
                randomWord.createWord(words.get(random_index));
                randomKey.createKey(keys.get(random_index));
                Log.e("Key==", keys.size() + " " + random_index + " " + answer_word);

                fetchClues();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
            // ....
        });
    }

    public void fetchClues() {
        ArrayList<Character> charList = new ArrayList<Character>();

        Firebase ref = new Firebase("https://pinoygenius.firebaseio.com/Clues");
        Log.e("Random Key", randomKey.getKey());
        Query queryRef = ref.orderByChild("word_id").equalTo(randomKey.getKey());
        queryRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Clues clues = dataSnapshot.getValue(Clues.class);
                clues_key = clues.getWord_id();
                clues_list = clues.getClues();

                for (int i = 0; i < clues_list.size(); i++) {
                    if (cluesDBAdapter.getRowCount() == 10) {
                        cluesDBAdapter.updateClue(clues_list.get(i).toString(), "true", String.valueOf(i));
                    } else
                        cluesDBAdapter.insertClue(clues_list.get(i).toString(), "true");
                    Log.e("Letter", clues_list.get(i).toString());
                }

                getClues();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    public ArrayList<Character> convertAnswerToList(String str) {
        ArrayList<Character> charList = new ArrayList<Character>();
        for (int i = 0; i < str.length(); i++) {
            charList.add(str.charAt(i));
        }

        long seed = System.nanoTime();
        Collections.shuffle(charList, new Random(seed));
        return charList;
    }

    public ArrayList<Character> convertAlphabetToList() {
        ArrayList<Character> charList = new ArrayList<Character>();
        for (int i = 0; i < alphabet.length; i++) {
            charList.add(alphabet[i]);
        }
        long seed = System.nanoTime();
        Collections.shuffle(charList, new Random(seed));
        return charList;
    }

    public ArrayList<Character> getLettersList(String word) {
        letterPrefs.clearAll();
        ArrayList<Character> letterList = new ArrayList<Character>();
        ArrayList<Character> charList = convertAnswerToList(word);

        for (int i =0; i < charList.size(); i++){
            Log.e("Answer list items", charList.get(i).toString());
        }
        charList.addAll(convertAlphabetToList());
        for (int i =0; i < charList.size(); i++){
            Log.e("Answer Combined", charList.get(i).toString());
        }
        Log.e("Size of combined list", charList.size() + " ");

        for (int i = 0; i < 16; i++) {
            letterList.add(charList.get(i));
            Log.e("List of letters", letterList.get(i).toString());
        }

        for (int i = 0; i < letterList.size(); i++) {
            letterPrefs.createLetter(letterList.get(i).toString());

            if (lettersDBAdapter.getRowCount() == 16) {
                lettersDBAdapter.updateLetter(letterList.get(i).toString(), "true", String.valueOf(i));
            } else
                lettersDBAdapter.insertLetter(letterList.get(i).toString(), "true");
            Log.e("Letter", letterList.get(i).toString());
        }

        long seed = System.nanoTime();
        Collections.shuffle(letterList, new Random(seed));
        Log.e("Letter size Shuffled", letterList.size() + " ");
        getValues();
        return letterList;
    }

    private List<String> getValues() {
        Log.e("Reading dab", "Long running");
        List<String> resultList = new ArrayList<String>();
        Cursor c = lettersDBAdapter.queryAllLetters();
        while (c.moveToNext()) {
            letter_name = c.getString(c.getColumnIndex(dbOpenHelper.KEY_LETTER));
            resultList.add(letter_name);
        }
        c.close();

        letters_list = resultList;
        wordGridAdapter = new WordGridAdapter(getActivity(), R.id.letter_btn, resultList);
        letters_grid.setAdapter(wordGridAdapter);
        ListViewHelper.setGridViewHeightBasedOnChildren(letters_grid, 4);
        wordGridAdapter.notifyDataSetChanged();
        letters_grid.invalidateViews();

        return resultList;
    }

    private List<String> getClues() {
        Log.e("Reading dab", "Long running");
        List<String> resultList = new ArrayList<String>();
        Cursor c = cluesDBAdapter.queryAllClues();
        while (c.moveToNext()) {
            clue_name = c.getString(c.getColumnIndex(dbOpenHelper.KEY_CLUE));
            resultList.add(clue_name);
        }
        c.close();

        clues_list = resultList;
        cluesGridAdapter = new CluesGridAdapter(getActivity(), R.id.clue_btn, clues_list);
        clues_grid.setAdapter(cluesGridAdapter);
        ListViewHelper.setGridViewHeightBasedOnChildren(clues_grid, 2);
        cluesGridAdapter.notifyDataSetChanged();
        clues_grid.invalidateViews();

        return resultList;
    }
}
