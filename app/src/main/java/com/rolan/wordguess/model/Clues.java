package com.rolan.wordguess.model;

import java.util.ArrayList;

/**
 * Created by Brayo on 4/2/2016.
 */
public class Clues {
    private String word_id;
    private ArrayList<String> clues = new ArrayList<>();

    public Clues() {
    }

    public String getWord_id() {
        return word_id;
    }

    public Clues setWord_id(String word_id) {
        this.word_id = word_id;
        return this;
    }

    public ArrayList<String> getClues() {
        return clues;
    }

    public Clues setClues(ArrayList<String> clues) {
        this.clues = clues;
        return this;
    }
}
