package com.rolan.wordguess.model;

/**
 * Created by Brayo on 4/21/2016.
 */
public class Message {
    private String id;
    private String message;
    private int type;
    private String created_at;
    private String updated_at;

    public static int MY_MESSAGE = 0;
    public static int NOT_MY_MESSAGE = 1;
    public static int DATE = 2;

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
