package com.rolan.wordguess.model;

/**
 * Created by Brayo on 4/2/2016.
 */
public class Quiz {

    private String word_id;
    private boolean correct;
    private boolean possible;
    private boolean wrong;
    private String guessor_id;
    private String describer_id;

    public Quiz() {
    }

    public String getWord_id() {
        return word_id;
    }

    public Quiz setWord_id(String word_id) {
        this.word_id = word_id;
        return this;
    }

    public boolean isCorrect() {
        return correct;
    }

    public Quiz setCorrect(boolean correct) {
        this.correct = correct;
        return this;
    }

    public boolean isPossible() {
        return possible;
    }

    public Quiz setPossible(boolean possible) {
        this.possible = possible;
        return this;
    }

    public boolean isWrong() {
        return wrong;
    }

    public Quiz setWrong(boolean wrong) {
        this.wrong = wrong;
        return this;
    }

    public String getGuessor_id() {
        return guessor_id;
    }

    public Quiz setGuessor_id(String guessor_id) {
        this.guessor_id = guessor_id;
        return this;
    }

    public String getDescriber_id() {
        return describer_id;
    }

    public Quiz setDescriber_id(String describer_id) {
        this.describer_id = describer_id;
        return this;
    }
}
