package com.rolan.wordguess.model;

/**
 * Created by Brayo on 4/2/2016.
 */
public class User {

    private String uid;
    private String name;
    private String gender;
    private String email;
    private String coins;
    private Boolean online = true;

    public User() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Boolean getOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public String getName() {
        if (!(name == null))
            return name;
        else
            return "Anon";
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public User setGender(String gender) {
        this.gender = gender;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getCoins() {
        return coins;
    }

    public User setCoins(String coins) {
        this.coins = coins;
        return this;
    }
}
