package com.rolan.wordguess.model;

import java.util.ArrayList;

/**
 * Created by Brayo on 4/2/2016.
 */
public class Word {
    private String word;
    private ArrayList<String> description_words = new ArrayList<>();


    public Word() {
    }

    public String getWord() {
        return word;
    }

    public Word setWord(String word) {
        this.word = word;
        return this;
    }

    public ArrayList<String> getDescription_words() {
        return description_words;
    }

    public Word setDescription_words(ArrayList<String> description_words) {
        this.description_words = description_words;
        return this;
    }
}
