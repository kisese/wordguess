package com.rolan.wordguess.pagermanager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.rolan.wordguess.main.fragments.GameActivityFrament;
import com.rolan.wordguess.main.fragments.SinglePlayerFragment;


/**
 * Created by Brayo on 3/25/2015.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    protected Context mContext;
    //ShoppingAdapter kisese.soft.evolva.com.restaurantpro.adapter;

    private final SparseArray<Fragment> mPageReferences = new SparseArray<Fragment>();

    CharSequence mTitles[];

    public MainPagerAdapter(FragmentManager fm, Context context, CharSequence mTitles[]) {
        super(fm);
        this.mTitles = mTitles;
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment  fragment = new SinglePlayerFragment();
        switch (position) {
            case 0:
                return new SinglePlayerFragment();
            case 1:
                return new GameActivityFrament();
        }

        return fragment;
    }
    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferences.remove(position);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        // POSITION_NONE makes it possible to reload the PagerAdapter
        return POSITION_NONE;
    }
}