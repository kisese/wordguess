package com.rolan.wordguess.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Brayo on 4/16/2016.
 */
public class LettersPref {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String SHARED_PREFER_FILE_NAME = "letters";

    public LettersPref(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(SHARED_PREFER_FILE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLetter(String word) {
        editor.putString(word, word);
        editor.commit();
    }

    public String[] getLetterArray(){
        String[] tags = pref.getAll().keySet().toArray(new String[0]);
        Arrays.sort(tags);
        return tags;
    }

    public String getLetter(){
        String word = pref.getString("word", null);
        return word;
    }

    public ArrayList<String> getLettersArraylist(){
        ArrayList<String> temp = new ArrayList<>();
        String[] tables = getLetterArray();

        for(int i = 0; i < tables.length; i++){
            temp.add(tables[i]);
        }

        return temp;
    }

    public void clearAll(){
        pref.edit().clear();
        editor.commit();
    }
}
