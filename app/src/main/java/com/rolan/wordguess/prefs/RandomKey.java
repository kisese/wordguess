package com.rolan.wordguess.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Brayo on 4/2/2016.
 */
public class RandomKey {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String SHARED_PREFER_FILE_NAME = "random_keys";

    public RandomKey(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(SHARED_PREFER_FILE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createKey(String Key) {
        editor.putString("key", Key);
        editor.commit();
    }

    public String[] getKeyArray(){
        String[] tags = pref.getAll().keySet().toArray(new String[0]);
        Arrays.sort(tags);
        return tags;
    }

    public String getKey(){
        String Key = pref.getString("key", null);
        return Key;
    }

    public ArrayList<String> getKeysArraylist(){
        ArrayList<String> temp = new ArrayList<>();
        String[] tables = getKeyArray();

        for(int i = 0; i < tables.length; i++){
            temp.add(tables[i]);
        }

        return temp;
    }
}
