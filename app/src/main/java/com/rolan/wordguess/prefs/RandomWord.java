package com.rolan.wordguess.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Brayo on 4/2/2016.
 */
public class RandomWord {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String SHARED_PREFER_FILE_NAME = "random_words";

    public RandomWord(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(SHARED_PREFER_FILE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createWord(String word) {
        editor.putString("word", word);
        editor.commit();
    }

    public String[] getWordArray(){
        String[] tags = pref.getAll().keySet().toArray(new String[0]);
        Arrays.sort(tags);
        return tags;
    }

    public String getWord(){
        String word = pref.getString("word", null);
        return word;
    }

    public ArrayList<String> getWordsArraylist(){
        ArrayList<String> temp = new ArrayList<>();
        String[] tables = getWordArray();

        for(int i = 0; i < tables.length; i++){
            temp.add(tables[i]);
        }

        return temp;
    }
}

