package com.rolan.wordguess.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.rolan.wordguess.util.GetDate;

/**
 * Created by Brayo on 4/17/2016.
 */
public class CluesDBAdapter {
    SQLiteDatabase database_ob;
    DBOpenHelper openHelper_ob;
    GetDate getDate = new GetDate();
    Context context;

    String[] cols = {openHelper_ob.KEY_ID, openHelper_ob.KEY_CLUE,
            openHelper_ob.KEY_WRITE};

    public CluesDBAdapter(Context context) {
        this.context = context;
    }

    public CluesDBAdapter openToRead() {
        openHelper_ob = DBOpenHelper.getInstance(context);
        database_ob = openHelper_ob.getReadableDatabase();
        return this;
    }

    public CluesDBAdapter openToWrite() {
        openHelper_ob = DBOpenHelper.getInstance(context);
        database_ob = openHelper_ob.getWritableDatabase();
        return this;
    }

    public void close_db() {
        database_ob.close();
    }

    public long insertClue(String letter, String write) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(openHelper_ob.KEY_CLUE, letter);
        contentValues.put(openHelper_ob.KEY_WRITE, write);
        openToWrite();
        long val = database_ob.insert(openHelper_ob.CLUES_TABLE, null,
                contentValues);
        //close_db();
        return val;
    }

    public Cursor queryAllClues() {
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.CLUES_TABLE, cols, null,
                null, null, null, openHelper_ob.KEY_ID + " ASC");
        // //close_db();
        return c;
    }

    public long updateClue(String letter, String write, String id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(openHelper_ob.KEY_CLUE, letter);
        contentValues.put(openHelper_ob.KEY_WRITE, write);
        openToWrite();
        long val = database_ob.update(openHelper_ob.CLUES_TABLE, contentValues,
                openHelper_ob.KEY_ID + " = " + id, null);
        //close_db();
        return val;
    }

    public Cursor querySingleClue(String filter) {
        String[] selectionArgs = {"%" + filter + "%"};
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.CLUES_TABLE, cols, openHelper_ob.KEY_ID + " LIKE ?",
                selectionArgs, null, null, null);
        ////close_db();
        return c;
    }

    public int deletSingleClue(String id) {
        // TODO Auto-generated method stub
        openToWrite();
        int val = database_ob.delete(openHelper_ob.CLUES_TABLE,
                openHelper_ob.KEY_ID + " = ?", new String[]{id});
        //close_db();
        return val;
    }

    public int deletAllClues() {
        // TODO Auto-generated method stub
        openToWrite();
        int val = database_ob.delete(openHelper_ob.CLUES_TABLE,
                null, null);
        //close_db();
        return val;
    }


    public int getRowCount() {
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.CLUES_TABLE, cols, null,
                null, null, null, null);
        int count = c.getCount();
        //close_db();
        return count;
    }
}
