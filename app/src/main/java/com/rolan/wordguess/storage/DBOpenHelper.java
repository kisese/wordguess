package com.rolan.wordguess.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Brayo on 4/16/2016.
 */
public class DBOpenHelper  extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "WORDS_DB";
    public static final int VERSION = 5;

    public static final String LETTERS_TABLE = "letters_table";
    public static final String CLUES_TABLE = "clues_table";
    public static final String MESSAGES_TABLE = "messages_table";
    public static final String USERS_TABLE = "users_table";

    public static final String KEY_ID = "_id";
    public static final String KEY_LETTER = "letter";
    public static final String KEY_WRITE = "write";

    public static final String KEY_CLUE = "letter";

    public static final String KEY_MESSAGE = "message";
    public static final String KEY_TYPE = "type";
    public static final String KEY_CREATED_AT = "created_at";
    public static final String KEY_UPDATED_AT = "updated_at";

    public static final String KEY_UID = "uid";
    public static final String KEY_NAME = "name";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_COINS = "coins";
    public static final String KEY_ONLINE = "online";

    public static final String SCRIPT_LETTERS_TABLE = "create table " + LETTERS_TABLE + " ("
            + KEY_ID + " integer primary key autoincrement not null, " +
            KEY_LETTER + " text not null, " +
            KEY_WRITE + " text not null);";

    public static final String SCRIPT_CLUES_TABLE = "create table " + CLUES_TABLE + " ("
            + KEY_ID + " integer primary key autoincrement not null, " +
            KEY_CLUE + " text not null, " +
            KEY_WRITE + " text not null);";

    public static final String SCRIPT_MESSAGES_TABLE = "create table " + MESSAGES_TABLE + " ("
            + KEY_ID + " integer primary key autoincrement not null, " +
            KEY_MESSAGE + " text not null, " +
            KEY_TYPE + " text not null, " +
            KEY_CREATED_AT + " text not null, " +
            KEY_UPDATED_AT + " text not null);";

    public static final String SCRIPT_USERS_TABLE = "create table " + USERS_TABLE + " ("
            + KEY_ID + " integer primary key autoincrement not null, " +
            KEY_UID + " text not null, " +
            KEY_NAME + " text , " +
            KEY_GENDER + " text , " +
            KEY_EMAIL + " text , " +
            KEY_COINS + " text , " +
            KEY_ONLINE + " text , " +
            KEY_CREATED_AT + " text , " +
            KEY_UPDATED_AT + " text );";

    private static DBOpenHelper instance;

    public static synchronized DBOpenHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DBOpenHelper(context.getApplicationContext());
        }
        return instance;
    }

    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SCRIPT_LETTERS_TABLE);
        db.execSQL(SCRIPT_CLUES_TABLE);
        db.execSQL(SCRIPT_MESSAGES_TABLE);
        db.execSQL(SCRIPT_USERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table " + LETTERS_TABLE);
        db.execSQL("drop table " + CLUES_TABLE);
        db.execSQL("drop table " + MESSAGES_TABLE);
        db.execSQL("drop table " + USERS_TABLE);
        onCreate(db);
    }
}
