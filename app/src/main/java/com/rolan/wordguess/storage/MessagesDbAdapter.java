package com.rolan.wordguess.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.rolan.wordguess.util.GetDate;

/**
 * Created by Brayo on 4/21/2016.
 */
public class MessagesDbAdapter {
    
    SQLiteDatabase database_ob;
    DBOpenHelper openHelper_ob;
    GetDate getDate = new GetDate();
    Context context;

    String[] cols = {openHelper_ob.KEY_ID, openHelper_ob.KEY_MESSAGE,
            openHelper_ob.KEY_TYPE, openHelper_ob.KEY_CREATED_AT, openHelper_ob.KEY_UPDATED_AT};

    public MessagesDbAdapter(Context context) {
        this.context = context;
    }

    public MessagesDbAdapter openToRead() {
        openHelper_ob = DBOpenHelper.getInstance(context);
        database_ob = openHelper_ob.getReadableDatabase();
        return this;
    }

    public MessagesDbAdapter openToWrite() {
        openHelper_ob = DBOpenHelper.getInstance(context);
        database_ob = openHelper_ob.getWritableDatabase();
        return this;
    }

    public void close_db() {
        database_ob.close();
    }

    public long insertMessage(String message, String type) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(openHelper_ob.KEY_MESSAGE, message);
        contentValues.put(openHelper_ob.KEY_TYPE, type);
        contentValues.put(openHelper_ob.KEY_CREATED_AT, getDate.getTimeStamp());
        contentValues.put(openHelper_ob.KEY_UPDATED_AT, getDate.getTimeStamp());
        openToWrite();
        long val = database_ob.insert(openHelper_ob.MESSAGES_TABLE, null,
                contentValues);
        //close_db();
        return val;
    }

    public Cursor queryAllMessages() {
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.MESSAGES_TABLE, cols, null,
                null, null, null, openHelper_ob.KEY_ID + " ASC");
        // //close_db();
        return c;
    }

    public long updateMessage(String message, String type, String id) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(openHelper_ob.KEY_MESSAGE, message);
        contentValues.put(openHelper_ob.KEY_TYPE, type);
        contentValues.put(openHelper_ob.KEY_CREATED_AT, getDate.getTimeStamp());
        contentValues.put(openHelper_ob.KEY_UPDATED_AT, getDate.getTimeStamp());
        openToWrite();
        long val = database_ob.update(openHelper_ob.MESSAGES_TABLE, contentValues,
                openHelper_ob.KEY_ID + " = " + id, null);
        //close_db();
        return val;
    }

    public Cursor querySingleMessage(String filter) {
        String[] selectionArgs = {"%" + filter + "%"};
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.MESSAGES_TABLE, cols, openHelper_ob.KEY_ID + " LIKE ?",
                selectionArgs, null, null, null);
        ////close_db();
        return c;
    }

    public int deletSingleMessage(String id) {
        // TODO Auto-generated method stub
        openToWrite();
        int val = database_ob.delete(openHelper_ob.MESSAGES_TABLE,
                openHelper_ob.KEY_ID + " = ?", new String[]{id});
        //close_db();
        return val;
    }

    public int deletAllMessages() {
        // TODO Auto-generated method stub
        openToWrite();
        int val = database_ob.delete(openHelper_ob.MESSAGES_TABLE,
                null, null);
        //close_db();
        return val;
    }


    public int getRowCount() {
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.MESSAGES_TABLE, cols, null,
                null, null, null, null);
        int count = c.getCount();
        //close_db();
        return count;
    }
}
