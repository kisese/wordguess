package com.rolan.wordguess.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.rolan.wordguess.util.GetDate;

/**
 * Created by Brayo on 4/24/2016.
 */
public class UsersDBAdapter {
    SQLiteDatabase database_ob;
    DBOpenHelper openHelper_ob;
    GetDate getDate = new GetDate();
    Context context;

    String[] cols = {openHelper_ob.KEY_ID, openHelper_ob.KEY_UID,
            openHelper_ob.KEY_NAME, openHelper_ob.KEY_GENDER, openHelper_ob.KEY_EMAIL,
            openHelper_ob.KEY_COINS, openHelper_ob.KEY_ONLINE, openHelper_ob.KEY_CREATED_AT,
            openHelper_ob.KEY_UPDATED_AT};

    public UsersDBAdapter(Context context) {
        this.context = context;
    }

    public UsersDBAdapter openToRead() {
        openHelper_ob = DBOpenHelper.getInstance(context);
        database_ob = openHelper_ob.getReadableDatabase();
        return this;
    }

    public UsersDBAdapter openToWrite() {
        openHelper_ob = DBOpenHelper.getInstance(context);
        database_ob = openHelper_ob.getWritableDatabase();
        return this;
    }

    public void close_db() {
        database_ob.close();
    }

    public long insertUser(String uid, String online) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(openHelper_ob.KEY_UID, uid);
        contentValues.put(openHelper_ob.KEY_ONLINE, online);
        contentValues.put(openHelper_ob.KEY_CREATED_AT, getDate.getTimeStamp());
        openToWrite();
        long val = database_ob.insert(openHelper_ob.USERS_TABLE, null,
                contentValues);
        //close_db();
        return val;
    }

    public Cursor queryAllUsers() {
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.USERS_TABLE, cols, null,
                null, null, null, openHelper_ob.KEY_ID + " ASC");
        // //close_db();
        return c;
    }

    public long updateUser(String uid,String online) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(openHelper_ob.KEY_ONLINE, online);
        contentValues.put(openHelper_ob.KEY_UPDATED_AT, getDate.getTimeStamp());
        openToWrite();
        long val = database_ob.update(openHelper_ob.USERS_TABLE, contentValues,
                openHelper_ob.KEY_UID + " = '" + uid + "'", null);
        //close_db();
        return val;
    }

    public long updateUserDEtails(String uid, String name, String gender, String email, String coins,
                           String online) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(openHelper_ob.KEY_NAME, name);
        contentValues.put(openHelper_ob.KEY_GENDER, gender);
        contentValues.put(openHelper_ob.KEY_EMAIL, email);
        contentValues.put(openHelper_ob.KEY_COINS, coins);
        contentValues.put(openHelper_ob.KEY_ONLINE, online);
        contentValues.put(openHelper_ob.KEY_UPDATED_AT, getDate.getTimeStamp());
        openToWrite();
        long val = database_ob.update(openHelper_ob.USERS_TABLE, contentValues,
                openHelper_ob.KEY_UID + " = '" + uid + "'", null);
        //close_db();
        return val;
    }

    public long updateUserProfile(String uid, String name, String gender, String email) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(openHelper_ob.KEY_NAME, name);
        contentValues.put(openHelper_ob.KEY_GENDER, gender);
        contentValues.put(openHelper_ob.KEY_EMAIL, email);
        contentValues.put(openHelper_ob.KEY_UPDATED_AT, getDate.getTimeStamp());
        openToWrite();
        long val = database_ob.update(openHelper_ob.USERS_TABLE, contentValues,
                openHelper_ob.KEY_UID + " = '" + uid + "'", null);
        //close_db();
        return val;
    }


    public Cursor querySingleUser(String filter) {
        String[] selectionArgs = {"%" + filter + "%"};
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.USERS_TABLE, cols, openHelper_ob.KEY_UID + " LIKE ?",
                selectionArgs, null, null, null);
        ////close_db();
        return c;
    }

    public int deletSingleUser(String id) {
        // TODO Auto-generated method stub
        openToWrite();
        int val = database_ob.delete(openHelper_ob.USERS_TABLE,
                openHelper_ob.KEY_ID + " = ?", new String[]{id});
        //close_db();
        return val;
    }

    public int deletAllUsers() {
        // TODO Auto-generated method stub
        openToWrite();
        int val = database_ob.delete(openHelper_ob.USERS_TABLE,
                null, null);
        //close_db();
        return val;
    }


    public int getRowCount() {
        openToRead();
        Cursor c = database_ob.query(openHelper_ob.USERS_TABLE, cols, null,
                null, null, null, null);
        int count = c.getCount();
        //close_db();
        return count;
    }
}
