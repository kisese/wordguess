package com.rolan.wordguess.user;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.Sharer;
import com.facebook.share.widget.ShareDialog;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.rolan.wordguess.R;
import com.rolan.wordguess.main.activity.MainActivity;
import com.rolan.wordguess.util.InputValidator;
import com.rolan.wordguess.util.JSONParser;
import com.rolan.wordguess.util.SessionManager;

import org.json.JSONObject;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {

    private final String PENDING_ACTION_BUNDLE_KEY =
            "com.enezaeducation.app:PendingAction";

    private PendingAction pendingAction = PendingAction.NONE;
    private boolean canPresentShareDialog;
    private boolean canPresentShareDialogWithPhotos;
    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    private ShareDialog shareDialog;
    private SessionManager sessionManager;
    private EditText edtxt_email, edtxt_password;
    private BootstrapButton btn_login;
    private String user_email, user_password;
    private DotProgressBar dotProgressBar;
    private TextView txt_email, txt_password;
    private Boolean fb_login = true;

    private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onCancel() {
            Log.d("EnezaFacebook", "Canceled");
        }

        @Override
        public void onError(FacebookException error) {
            Log.d("EnezaFacebook", String.format("Error: %s", error.toString()));
            String title = "An Error Occured";
            String alertMessage = error.getMessage();
            showResult(title, alertMessage);
        }

        @Override
        public void onSuccess(Sharer.Result result) {
            Log.d("EnezaFacebook", "Success!");
            if (result.getPostId() != null) {
                String title = "Success";
                String id = result.getPostId();
                String alertMessage = getString(R.string.successfully_posted_post, id);
                showResult(title, alertMessage);
            }
        }

        private void showResult(String title, String alertMessage) {
            new AlertDialog.Builder(LoginActivity.this)
                    .setTitle(title)
                    .setMessage(alertMessage)
                    .setPositiveButton(R.string.ok, null)
                    .show();
        }
    };
    private LoginButton loginButton;
    private String fb_user_gender;
    private Firebase ref;
    private String email, name;
    private InputValidator inputValidator;

    private enum PendingAction {
        NONE,
        POST_PHOTO,
        POST_STATUS_UPDATE
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        ref = new Firebase("https://pinoygenius.firebaseio.com/");

        loginButton = (LoginButton) findViewById(R.id.facebook_login);
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday"));
        callbackManager = CallbackManager.Factory.create();
        sessionManager = new SessionManager(this);
        inputValidator = new InputValidator();

        edtxt_email = (EditText) findViewById(R.id.edtxt_email);
        edtxt_password = (EditText) findViewById(R.id.edtxt_password);
        btn_login = (BootstrapButton) findViewById(R.id.btn_login);
        dotProgressBar = (DotProgressBar) findViewById(R.id.dot_progress_bar);
        txt_email = (TextView) findViewById(R.id.txt_email);
        txt_password = (TextView) findViewById(R.id.txt_password);

        dotProgressBar.setVisibility(View.GONE);

        if (sessionManager.isUserLoggedIn()) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
        }


        String possibleEmail = "";
        ;
        Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) {
            possibleEmail = account.name;

        }
        edtxt_email.setText(possibleEmail);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_email = edtxt_email.getText().toString();
                user_password = edtxt_password.getText().toString();
                fb_login = false;

                if ((user_email.length() > 0) && (user_password.length() > 0)) {
                    if (inputValidator.validEmail(user_email)) {
                        dotProgressBar.setVisibility(View.VISIBLE);
                        hideViews();
                        Toast.makeText(LoginActivity.this, "Logging you in", Toast.LENGTH_LONG).show();
                        ref.createUser(user_email, user_password, new Firebase.ResultHandler() {
                            @Override
                            public void onSuccess() {
                                ref.authWithPassword(user_email, user_password, null);
                            }

                            @Override
                            public void onError(FirebaseError firebaseError) {
                                //create a user if he doesnt exist
                                ref.authWithPassword(user_email, user_password, null);
                            }
                        });
                    } else {
                        edtxt_email.setError("Please enter a valid email");
                    }
                }
            }
        });


        loginButton.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        fb_login = true;
                        handlePendingAction();
                        onFacebookAccessTokenChange(loginResult.getAccessToken());
                        // App code
                        Toast.makeText(LoginActivity.this, "Succesful a", Toast.LENGTH_SHORT).show();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        // Application code
                                        //Log.e("Json Response", object.toString());
                                        Log.e("Login :::: <response> ", response.toString());
                                        Log.e("Login :::: <object> ", object.toString());
                                        Toast.makeText(LoginActivity.this, "Succesful", Toast.LENGTH_SHORT).show();
                                        JSONParser jsonParser = new JSONParser();
                                        try {
                                            if (!(object == null)) {
                                                updateUI();
                                                email = object.getString("email");
                                                name = object.getString("name");
                                                Toast.makeText(LoginActivity.this, "Just a moment...", Toast.LENGTH_SHORT).show();
                                            } else
                                                Toast.makeText(LoginActivity.this, "Please try that again", Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            Toast.makeText(LoginActivity.this, "Please try that again", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        if (pendingAction != PendingAction.NONE) {
                            showAlert();
                            pendingAction = PendingAction.NONE;
                        }
                        //updateUI();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (pendingAction != PendingAction.NONE
                                && exception instanceof FacebookAuthorizationException) {
                            showAlert();
                            pendingAction = PendingAction.NONE;
                        }
                        //updateUI();
                    }


                    private void showAlert() {
                        new AlertDialog.Builder(LoginActivity.this)
                                .setTitle(R.string.cancelled)
                                .setMessage(R.string.permission_not_granted)
                                .setPositiveButton(R.string.ok, null)
                                .show();
                    }
                });

        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(
                callbackManager,
                shareCallback);

        if (savedInstanceState != null) {
            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
            pendingAction = PendingAction.valueOf(name);
        }

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                updateUI();
                // It's possible that we were waiting for Profile to be populated in order to
                // post a status update.
                handlePendingAction();
            }
        };

            ref.addAuthStateListener(new Firebase.AuthStateListener() {
                @Override
                public void onAuthStateChanged(AuthData authData) {
                    if (authData != null) {
                        name = ((String) authData.getProviderData().get("email"));
                        dotProgressBar.setVisibility(View.GONE);
                        Toast.makeText(LoginActivity.this, "User is logged in", Toast.LENGTH_LONG).show();
                        sessionManager.createUserLoginSession(user_email, user_email, authData.getUid());
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(i);
                    } else {
                        showViews();
                        Toast.makeText(LoginActivity.this, "Oops and error occured", Toast.LENGTH_LONG).show();
                    }
                }
            });

    }


    private void onFacebookAccessTokenChange(AccessToken token) {
        if (token != null) {
            ref.authWithOAuthToken("facebook", token.getToken(), new Firebase.AuthResultHandler() {
                @Override
                public void onAuthenticated(AuthData authData) {
                    // The Facebook user is now authenticated with your Firebase app
                    Toast.makeText(LoginActivity.this, "User is logged in", Toast.LENGTH_LONG).show();
                    sessionManager.createUserLoginSession(email, name, authData.getUid());
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                }

                @Override
                public void onAuthenticationError(FirebaseError firebaseError) {
                    // there was an error
                }
            });
        } else {
        /* Logged out of Facebook so do a logout from the Firebase app */
            ref.unauth();
        }
    }

    private void updateUI() {
        Profile profile = Profile.getCurrentProfile();
        if (profile != null) {
            //fb_name.setText(profile.getName());
            // fb_picture.setProfileId(profile.getId());
            Toast.makeText(LoginActivity.this, "Logged in as " + profile.getName(), Toast.LENGTH_LONG).show();

//            CheckFBUserAsync check_user = new CheckFBUserAsync(LoginActivity.this);
//            String fb_id = profile.getId();
//            String fb_name = profile.getName();
//            check_user.execute(fb_id, fb_name);

        }
    }

    private void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case NONE:
                break;
        }
    }

    protected void onResume() {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.activateApp(this);

        updateUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Call the 'deactivateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onPause methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        profileTracker.stopTracking();
    }

    public void showViews() {
        edtxt_email.setVisibility(View.VISIBLE);
        edtxt_password.setVisibility(View.VISIBLE);
        btn_login.setVisibility(View.VISIBLE);
        txt_email.setVisibility(View.VISIBLE);
        txt_password.setVisibility(View.VISIBLE);
        loginButton.setVisibility(View.VISIBLE);
    }

    public void hideViews() {
        edtxt_email.setVisibility(View.GONE);
        edtxt_password.setVisibility(View.GONE);
        btn_login.setVisibility(View.GONE);
        txt_email.setVisibility(View.GONE);
        txt_password.setVisibility(View.GONE);
        loginButton.setVisibility(View.GONE);
    }
}
