package com.rolan.wordguess.user;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.afollestad.materialdialogs.MaterialDialog;
import com.beardedhen.androidbootstrap.BootstrapButton;
import com.firebase.client.Firebase;
import com.rolan.wordguess.R;
import com.rolan.wordguess.main.activity.MainActivity;
import com.rolan.wordguess.model.User;
import com.rolan.wordguess.storage.UsersDBAdapter;
import com.rolan.wordguess.util.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    private ArrayList<String> gender_list = new ArrayList<>();
    private EditText edtxt_name;
    private Spinner gender_spinner;
    private String gender, name;
    private BootstrapButton save_details;
    private SessionManager sessionManager;
    private UsersDBAdapter usersDBAdapter;
    private String uid, email;
    private HashMap<String, String> user = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Firebase.setAndroidContext(this);
        setSupportActionBar(toolbar);

        edtxt_name = (EditText) findViewById(R.id.edtxt_name);
        gender_spinner = (Spinner) findViewById(R.id.gender_spinner);
        save_details = (BootstrapButton) findViewById(R.id.btn_save_details);

        sessionManager = new SessionManager(this);
        usersDBAdapter = new UsersDBAdapter(this);

        user = sessionManager.getUserDetails();
        uid = user.get(SessionManager.KEY_UID);
        email = user.get(SessionManager.KEY_EMAIL);
        name = user.get(SessionManager.KEY_NAME);

        gender_list.add("Male");
        gender_list.add("Female");

        if (name.length() > 0)
            edtxt_name.setText(name);

        String[] array = gender_list.toArray(new String[gender_list.size()]);

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender_spinner.setAdapter(categoryAdapter);
        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = gender_list.get(position);
                gender = item;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        save_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = edtxt_name.getText().toString();

                if (name.length() > 0) {
                    final MaterialDialog dialog = new MaterialDialog.Builder(RegistrationActivity.this)
                            .title("Saving..")
                            .content("Saving your details")
                            .progress(true, 0)
                            .show();

                    if ((gender.length() > 0) && (name.length() > 0)) {

                        Firebase ref = new Firebase("https://pinoygenius.firebaseio.com/Users");

                        Firebase userRef = ref.child(uid);
                        Map<String, Object> details = new HashMap<String, Object>();
                        details.put("name", name);
                        details.put("gender", gender);
                        details.put("email", email);
                        userRef.updateChildren(details);

                        usersDBAdapter.updateUserProfile(uid, name, gender, email);
                    }

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            dialog.dismiss();

                            Intent i = new Intent(RegistrationActivity.this, MainActivity.class);
                            startActivity(i);
                        }

                    }, 2000); // 5000ms delay
                }
            }
        });
    }
}
